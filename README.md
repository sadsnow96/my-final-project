+ 5 file screenshot lại kết quả chạy của các bài thực hành về Selenium Grid, Jenkins và report lại test
    - screenshot máy hub: https://gitlab.com/sadsnow96/final-test/-/blob/master/grid-hub.png
    - screenshot máy node: https://gitlab.com/sadsnow96/final-test/-/blob/master/grid-node.png
    - screenshot report run song song FF and chrome: https://gitlab.com/sadsnow96/final-test/-/blob/master/pic-Parallel-Report.png
    - screenshot report run Final Project: https://gitlab.com/sadsnow96/final-test/-/blob/master/pic-Report-All-Test.png
    - screenshot Jenkins: https://gitlab.com/sadsnow96/final-test/-/blob/master/pic-Test-With-Jenkins.png
    - Jenkins: chạy thử 1 vài bài test trong Final Project. cmd được dùng để chạy test trong Jenkins là " -Dtest=FinalProject test"
        Jenkins acc: 
            -username: sadsnow96
            -password: tuanpx9898

+ Final Project: https://gitlab.com/sadsnow96/my-final-project/-/blob/master/src/test/java/com/selenium/test/FinalTest.java

+ Grid Test: https://gitlab.com/sadsnow96/my-final-project/-/blob/master/src/test/java/com/selenium/test/GridTest.java

+ chạy song song FF and chrome: https://gitlab.com/sadsnow96/my-final-project/-/blob/master/src/test/java/com/selenium/test/parallel.java
